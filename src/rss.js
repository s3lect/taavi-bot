#!/usr/bin/env node
const fs = require('fs-extra');
const path = require('path');
const Matrix = require('matrix-js-sdk');
const {
	LocalStorageCryptoStore,
} = require('matrix-js-sdk/lib/crypto/store/localStorage-crypto-store');
const { LocalStorage } = require('node-localstorage');
global.Olm = require('./olm');
path.join(__dirname, './olm.wasm');
const Parser = require('rss-parser');

const parser = new Parser();
const { inquirerInput, inquirerChooseAction } = require('./inquirer');
const { getCredentialsWithPassword } = require('./matrix');
const homeDir = require('os').homedir();
const { name } = require('../package.json');
const dataDir = path.join(homeDir, '.local', 'share', name, 'rss');
fs.ensureDirSync(dataDir);
const configDir = path.join(homeDir, '.config', name, 'rss');
fs.ensureDirSync(configDir);

async function parseRss({ rssUrl, domain }) {
	const rssDataDir = path.join(dataDir, domain);
	fs.ensureDirSync(rssDataDir);
	const itemsDataPath = path.join(rssDataDir, 'data.json');
	if (!fs.existsSync(itemsDataPath)) fs.writeFileSync(itemsDataPath, '[]');
	const oldItems = require(itemsDataPath);
	const oldItemsIndex = new Set(oldItems.map(({ link }) => link));
	try {
		let feed = await parser.parseURL(rssUrl);
		console.log(feed.title);
		const items = feed.items
			.map(({ contentSnippet, title, link }) => ({ contentSnippet, title, link }))
			.filter(({ link }) => !oldItemsIndex.has(link));
		console.log('items', items);
		const newItems = [...items, ...oldItems];
		fs.writeFileSync(itemsDataPath, JSON.stringify(newItems, null, 2));
		await sendMatrixMessage({ domain, items: newItems });
	} catch (error) {
		console.log(error);
	}
}

async function sendMatrixMessage({ domain, items }) {
	// check if there are new posts that should be send to matrix
	const itemNotSend = items.find(({ eventId }) => !eventId);
	if (!itemNotSend) return;
	console.log('sending item', itemNotSend);
	// get the login credentials
	const credentialsFilePath = path.join(configDir, `${domain}.json`);
	const { server, accessToken, userId, deviceId, roomId } = require(credentialsFilePath);
	const itemsDataDir = path.join(dataDir, domain);
	const localStorage = new LocalStorage(itemsDataDir);
	const client = Matrix.createClient({
		baseUrl: `https://${server}`,
		accessToken,
		sessionStore: new Matrix.WebStorageSessionStore(localStorage),
		cryptoStore: new LocalStorageCryptoStore(localStorage),
		userId,
		deviceId,
	});

	// wait for sync event and send the post to matrix
	client.on('sync', async function (state, prevState, res) {
		if (state !== 'PREPARED') return;
		client.setGlobalErrorOnUnknownDevices(false);
		const message = `${itemNotSend.title}

${itemNotSend.contentSnippet}
${itemNotSend.link}`;
		const event = await client.sendTextMessage(roomId, message);
		itemNotSend.eventId = event.event_id;
		const itemsDataPath = path.join(itemsDataDir, 'data.json');
		fs.writeFileSync(itemsDataPath, JSON.stringify(items, null, 2));
		client.stopClient();
		process.exit(0);
	});
	await client.initCrypto();
	await client.startClient({ initialSyncLimit: 1 });
}

// Collect the rssURL and login data and try to login,
// if sucessful save the credentials.
async function loginUi() {
	const rssUrl = await inquirerInput('RSS feed url ', 'The URL was empty, please try again.');
	const domain = rssUrl.split('//')[1].split('/')[0];
	const server = await inquirerInput(
		'Your home server URL https://',
		'Your server URL was empty, please try again.',
		'matrix.org'
	);
	const username = await inquirerInput('Username', 'Your userID was empty, please try again.');
	const password = await inquirerInput('Password', 'Your password was empty, please try again.');
	const roomId = await inquirerInput('Room ID', 'Your room id was empty, please try again.');
	const { accessToken, userId, deviceId } = await getCredentialsWithPassword(
		server,
		username,
		password
	);
	const credentials = { rssUrl, domain, roomId, server, accessToken, userId, deviceId };
	const credentialsFilePath = path.join(configDir, `${domain}.json`);
	fs.writeFileSync(credentialsFilePath, JSON.stringify(credentials, null, 2));
}

// Delete an existing bot configuration
async function deleteUi() {
	const configs = fs.readdirSync(configDir).map((file) => {
		const config = require(path.join(configDir, file));
		return {
			name: config.domain,
			callback() {
				const credentialsFilePath = path.join(configDir, `${config.domain}.json`);
				fs.unlinkSync(credentialsFilePath);
				const itemsDataDir = path.join(dataDir, config.domain);
				fs.rmdirSync(itemsDataDir, { recursive: true });
			},
		};
	});
	if (!configs.length) {
		console.log('There are not bots configured. Create one now.');
		await loginUi();
		return;
	}
	await inquirerChooseAction('Which RSS feed config do you want to delete?', configs);
}

// Console User Interface
async function run() {
	if (process.argv.includes('-c')) {
		await inquirerChooseAction('What do you want to do?', [
			{ name: 'Create a new bot', callback: loginUi },
			{ name: 'Delete existing bots', callback: deleteUi },
		]);
	} else if (process.argv.includes('-r')) {
		for (file of fs.readdirSync(configDir)) {
			const config = require(path.join(configDir, file));
			await parseRss(config);
		}
	} else {
		const capitalizeFirstLetter = (string) => `${string.charAt(0).toUpperCase()}${string.slice(1)}`;
		console.log(`${capitalizeFirstLetter(
			name
		)} rssfeed bot - parse rss feeds and post them to matrix

Options:
 -c		interactive bot configuration
 -r		run all configured bots
`);
	}
}

run().catch((error) => console.warn(error));
