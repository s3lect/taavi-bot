const Matrix = require('matrix-js-sdk');

async function getCredentialsWithPassword(server, username, password) {
	const _credentials = await Matrix.createClient(`https://${server}`).loginWithPassword(
		username,
		password
	);
	return {
		accessToken: _credentials.access_token,
		userId: _credentials.user_id,
		deviceId: _credentials.device_id,
	};
}

module.exports = {
	getCredentialsWithPassword,
}
