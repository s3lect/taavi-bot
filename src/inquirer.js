const inquirer = require('inquirer');

async function inquirerInput(message, errorMessage, defaultValue) {
	const { answer } = await inquirer.prompt([
		{
			type: 'input',
			name: 'answer',
			default: defaultValue,
			message,
			validate(answer) {
				if (answer.length < 1) return errorMessage;
				return true;
			},
		},
	]);
	return answer;
}

async function inquirerChooseAction(message, actions) {
	const { startAction } = await inquirer.prompt([
		{
			type: 'list',
			message,
			name: 'startAction',
			choices: actions, // [{ name: 'Start the dev server' }, { name: 'Run production build' }],
		},
	]);

	await actions.find(({ name }) => name === startAction).callback();
}




module.exports = {
	inquirerInput,
	inquirerChooseAction,
}
