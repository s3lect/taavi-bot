#!/usr/bin/env node
const got = require('got');
const fs = require('fs-extra');
const path = require('path');
const Matrix = require('matrix-js-sdk');
const {
	LocalStorageCryptoStore,
} = require('matrix-js-sdk/lib/crypto/store/localStorage-crypto-store');
const { LocalStorage } = require('node-localstorage');
global.Olm = require('./olm');
path.join(__dirname, './olm.wasm');

const { inquirerInput, inquirerChooseAction } = require('./inquirer');
const { getCredentialsWithPassword } = require('./matrix');
const homeDir = require('os').homedir();
const { name } = require('../package.json');
const dataDir = path.join(homeDir, '.local', 'share', name, 'reddit-videos');
fs.ensureDirSync(dataDir);
const configDir = path.join(homeDir, '.config', name, 'reddit');
fs.ensureDirSync(configDir);

async function parseReddit({ subreddit, minUpvotes }) {
	const subredditDataDir = path.join(dataDir, subreddit);
	fs.ensureDirSync(subredditDataDir);
	const subredditDataPath = path.join(subredditDataDir, 'data.json');
	if (!fs.existsSync(subredditDataPath)) fs.writeFileSync(subredditDataPath, '[]');
	const oldItems = require(subredditDataPath);
	const oldItemsIndex = new Set(oldItems.map(({ url }) => url));
	try {
		const url = `https://www.reddit.com/r/${subreddit}/top/.json?count=20`;
		const response = await got(url).json();
		const items = response.data.children
			.map(
				({
					data: {
						ups,
						title,
						url,
						created,
						is_reddit_media_domain,
						secure_media,
						thumbnail,
						thumbnail_width,
						thumbnail_height,
					},
				}) => ({
					ups,
					title,
					videoData: secure_media ? secure_media.reddit_video : undefined,
					thumbnail: {
						url: thumbnail,
						width: thumbnail_width,
						height: thumbnail_height,
					},
					url,
					// secure_media.reddit_video.fallback_url
					created,
				})
			)
			.filter(({ ups, url }) => ups > minUpvotes && !oldItemsIndex.has(url));
		const redditItems = [...items, ...oldItems];
		fs.writeFileSync(subredditDataPath, JSON.stringify(redditItems, null, 2));
		await sendMatrixMessage({ subreddit, redditItems });
	} catch (error) {
		console.log(error);
	}
}

async function sendMatrixMessage({ subreddit, redditItems }) {
	// check if there are new posts that should be send to matrix
	const itemNotSend = redditItems.find(({ eventId }) => !eventId);
	if (!itemNotSend) return;
	console.log('sending item', itemNotSend);
	// get the login credentials
	const credentialsFilePath = path.join(configDir, `${subreddit}.json`);
	const { server, accessToken, userId, deviceId, roomId } = require(credentialsFilePath);
	const subredditDataDir = path.join(dataDir, subreddit);
	const localStorage = new LocalStorage(subredditDataDir);
	const client = Matrix.createClient({
		baseUrl: `https://${server}`,
		accessToken,
		sessionStore: new Matrix.WebStorageSessionStore(localStorage),
		cryptoStore: new LocalStorageCryptoStore(localStorage),
		userId,
		deviceId,
	});

	// wait for sync event and send the post to matrix
	client.on('sync', async function (state, prevState, res) {
		if (state !== 'PREPARED') return;
		client.setGlobalErrorOnUnknownDevices(false);
		const message = `${itemNotSend.title} ${itemNotSend.url}`;
		let event;
		if (itemNotSend.videoData) {
			const { videoData, title, thumbnail } = itemNotSend;
			event = await client.sendEvent(
				roomId,
				'm.room.message',
				{
					body: title,
					url: videoData.fallback_url,
					info: {
						duration: videoData.duration,
						h: videoData.height,
						w: videoData.width,
						mimetype: 'video/mp4',
						size: 1563685,
						thumbnail_info: {
							w: thumbnail.width,
							h: thumbnail.height,
							mimetype: 'image/jpeg',
							size: 46144,
						},
						thumbnail_url: thumbnail.url,
					},
					msgtype: 'm.video',
				},
				''
			);
		} else {
			event = await client.sendTextMessage(roomId, message);
		}
		const newRedditItems = redditItems.map((item) => {
			if (item.url === itemNotSend.url)
				return Object.assign(itemNotSend, { eventId: event.event_id });
			return item;
		});
		const subredditDataPath = path.join(subredditDataDir, 'data.json');
		// fs.writeFileSync(subredditDataPath, JSON.stringify(newRedditItems, null, 2));
		client.stopClient();
		process.exit(0);
	});
	await client.initCrypto();
	await client.startClient({ initialSyncLimit: 1 });
}

// Collect the subreddid and login data and try to login,
// if sucessful save the credentials.
async function loginUi() {
	const subreddit = await inquirerInput(
		'Subreddit /r/',
		'The subreddit name was empty, please try again.'
	);
	const minUpvotes = parseInt(
		await inquirerInput(
			'Minimum number of upvotes',
			'The upvote field was empty, please try again.',
			100
		),
		10
	);
	const server = await inquirerInput(
		'Your home server URL https://',
		'Your server URL was empty, please try again.',
		'matrix.org'
	);
	const username = await inquirerInput('Username', 'Your userID was empty, please try again.');
	const password = await inquirerInput('Password', 'Your password was empty, please try again.');
	const roomId = await inquirerInput('Room ID', 'Your room id was empty, please try again.');
	const { accessToken, userId, deviceId } = await getCredentialsWithPassword(
		server,
		username,
		password
	);
	const credentials = { subreddit, minUpvotes, roomId, server, accessToken, userId, deviceId };
	const credentialsFilePath = path.join(configDir, `${subreddit}.json`);
	fs.writeFileSync(credentialsFilePath, JSON.stringify(credentials, null, 2));
}

// Delete an existing bot configuration
async function deleteUi() {
	const configs = fs.readdirSync(configDir).map((file) => {
		const config = require(path.join(configDir, file));
		return {
			name: config.subreddit,
			callback() {
				const credentialsFilePath = path.join(configDir, `${config.subreddit}.json`);
				fs.unlinkSync(credentialsFilePath);
				const subredditDataDir = path.join(dataDir, config.subreddit);
				fs.rmdirSync(subredditDataDir, { recursive: true });
			},
		};
	});
	if (!configs.length) {
		console.log('There are not bots configured. Create one now.');
		await loginUi();
		return;
	}
	await inquirerChooseAction('Which subreddit config do you want to delete?', configs);
}

// Console User Interface
async function run() {
	if (process.argv.includes('-c')) {
		await inquirerChooseAction('What do you want to do?', [
			{ name: 'Create a new bot', callback: loginUi },
			{ name: 'Delete existing bots', callback: deleteUi },
		]);
	} else if (process.argv.includes('-r')) {
		for (file of fs.readdirSync(configDir)) {
			const config = require(path.join(configDir, file));
			await parseReddit(config);
		}
	} else {
		const capitalizeFirstLetter = (string) =>
			`${string.charAt(0).toUpperCase()}${string.slice(1)}`;
		console.log(`${capitalizeFirstLetter(name)} reddit video bot - parse reddit videos

Options:
 -c		interactive bot configuration
 -r		run all configured bots
`);
	}
}

run().catch((error) => console.warn(error));
