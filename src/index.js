#!/usr/bin/env node
const fs = require('fs-extra');
const path = require('path');
const puppeteer = require('puppeteer');

const Matrix = require('matrix-js-sdk');
const {
	LocalStorageCryptoStore,
} = require('matrix-js-sdk/lib/crypto/store/localStorage-crypto-store');
const { LocalStorage } = require('node-localstorage');
const { inquirerInput } = require('./inquirer');
global.Olm = require('./olm');

const { getCredentialsWithPassword } = require('./matrix');
const homeDir = require('os').homedir();
const { name } = require('../package.json');

const configDir = path.join(homeDir, '.config', name);
fs.ensureDirSync(configDir);
const credentialsFilePath = path.join(configDir, 'credentials.json');
const localStorage = new LocalStorage(configDir);

function closeBrowser(browser, answer) {
	browser.close();
	return answer;
}
// Debug questions
// is the eiffel tower taller than the tower of pisa?
async function queryGoogle(query) {
	console.log('query', query);
	const browser = await puppeteer.launch({ executablePath: '/snap/bin/chromium' });
	const page = await browser.newPage();
	await page.goto(`https://www.google.com/search?hl=en_US&q=${encodeURIComponent(query)}`);

	let answer = '';

	try {
		// did you mean
		const youMeant = await page.$eval('a.gL9Hy', ($el) => $el.textContent);
		if (youMeant) {
			browser.close();
			return queryGoogle(youMeant);
		}
	} catch (error) {}

	const bodyHandle = await page.$('body');
	answer = await page.evaluate((body) => {
		// list of people/movies/ ... with images
		if (body.querySelector('.dAassd')) {
			return [...body.querySelectorAll('.dAassd')]
				.map(($el) => {
					return [...$el.children]
						.slice(0, $el.children.length - 1)
						.map(($c) => $c.textContent.trim())
						.join(' ');
				})
				.join(' \n- ');
		}
		// tables like version numbers
		else if (body.querySelector('.webanswers-webanswers_table__webanswers-table')) {
			return [...body.querySelectorAll('.webanswers-webanswers_table__webanswers-table tr')]
				.map(($el) => {
					return [...$el.children].map(($c) => $c.textContent.trim()).join(', ');
				})
				.join(' \n- ');
		} else return '';
	}, bodyHandle);
	if (answer) return closeBrowser(browser, answer);

	const selectors = [
		['.H1u2de a', ($el) => `${$el.textContent} ${$el.href}`], // youtube video
		['.qv3Wpe', ($el) => $el.textContent], // Math ( eg: log_2(3) * pi^e )
		['.dDoNo', ($el) => $el.textContent], // money conversion results
		['div.zCubwf', ($el) => $el.textContent], // Basic Answers ( eg: christmas day )
		['div.XcVN5d', ($el) => $el.textContent], // Rich Answers ( eg: elevation of mount everest )
		['.hgKElc', ($el) => $el.textContent], // Featured Snippets ( eg: who is garfield )
		['.bbVIQb', ($el) => $el.textContent], // Lyrics ( eg: gecgecgec lyrics )
		['.UQt4rd', ($el) => $el.textContent], // Weather ( eg: weather new york)
		['#NotFQb', ($el) => $el.textContent], // Units Conversion ( eg: 1m into 1 cm )
		['.XcVN5d', ($el) => $el.textContent], // Translate ( eg: Vais para cascais? em ingles )
		// ['.'] // Knowledge Graph - right ( eg: the office )
	];
	for ([selector, callback] of selectors) {
		try {
			answer = await page.$eval(selector, callback);
			if (answer) return closeBrowser(browser, answer);
		} catch (error) {
			console.log('next ', error.message);
			console.log(' ');
		}
	}
	await page.screenshot({ path: 'screenshot.png' });
	return closeBrowser(browser, 'Sorry, I do not know that ¯_(ツ)_/¯');
}

// testbot42:matrix.org:
async function runMatrixClient() {
	if (!fs.existsSync(credentialsFilePath)) await loginUi();
	console.log(`Using credentials from ${credentialsFilePath}`);
	const { server, accessToken, userId, deviceId, username } = require(credentialsFilePath);
	const chatQueryRexEx = new RegExp(`^${username}[^ ]* (.*)`, 'i'); // /^testbot42[^ ]* (.*)/;
	const client = Matrix.createClient({
		baseUrl: `https://${server}`,
		accessToken,
		sessionStore: new Matrix.WebStorageSessionStore(localStorage),
		cryptoStore: new LocalStorageCryptoStore(localStorage),
		userId,
		deviceId,
	});
	let isReady = false;
	client.on('sync', async function (state, prevState, res) {
		if (state !== 'PREPARED') return;
		client.setGlobalErrorOnUnknownDevices(false);
		isReady = true;
	});

	client.on('Room.timeline', async (message, room) => {
		if (!isReady) return;
		let body = '';
		// console.log('message.event', message.event);
		try {
			if (message.event.type === 'm.room.encrypted') {
				const event = await client._crypto.decryptEvent(message);
				({ body } = event.clearEvent.content);
			} else {
				({ body } = message.event.content);
			}
			if (body && chatQueryRexEx.test(body)) {
				const match = chatQueryRexEx.exec(body);
				await client.sendTyping(message.event.room_id, true, 500);
				const answer = await queryGoogle(match[1].trim());
				client.sendTextMessage(message.event.room_id, answer);
			} else {
				console.log('#### message did not match: ', body);
			}
		} catch (error) {
			console.error('#### ', error);
		}
	});
	await client.initCrypto();
	await client.startClient({ initialSyncLimit: 1 });
}

async function loginUi() {
	const chromiumPath = await inquirerInput(
		'Path to chromium',
		'Your path to chromium was empty, please try again.',
		'/snap/bin/chromium'
	);
	const server = await inquirerInput(
		'Your home server URL https://',
		'Your server URL was empty, please try again.',
		'matrix.org'
	);
	const username = await inquirerInput('Username', 'Your userID was empty, please try again.');
	const password = await inquirerInput('Password', 'Your password was empty, please try again.');
	await getCredentialsWithPassword(server, username, password, chromiumPath);
	const { accessToken, userId, deviceId } = await getCredentialsWithPassword(
		server,
		username,
		password
	);
	const credentials = { chromiumPath, server, username, accessToken, userId, deviceId };
	fs.writeFileSync(credentialsFilePath, JSON.stringify(credentials, null, 2));
}

async function queryUi() {
	const query = await inquirerInput(
		'What do you want to know?',
		'Your question was empty, please try again.'
	);
	return await queryGoogle(query);
}

// startUi()
runMatrixClient()
	.then((a) => console.log(a))
	.catch((error) => console.error(error));
