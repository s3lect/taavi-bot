# Taavi

Taavi is a open Q & A chat bot for the Matrix protocol with e2e encryption. You can ask it any question and it will try to answer it.

## About The Project

![](https://i.imgur.com/4ve1n0D.png)
Taavi is a simple screen scraping bot that uses google to answer any question you ask it. Sure you could google it but, what's the fun in that?

### Built With

* [Google](https://www.google.com/)
* [Puppeteer](https://github.com/puppeteer/puppeteer)
* [Matrix Javascript SDK](https://github.com/matrix-org/matrix-js-sdk)

## Getting Started

To get the bot up and running you will need to **create an account** on your favorite Matrix server. Keep the server URL, username and password ready for the login. 

[https://app.element.io/](https://app.element.io/) (or any other Matrix client)

After creating the account, **join all Matrix rooms** where your bot should be present to answer questions. You can change rooms later on by logging into the bot account to leave previous rooms (or direct message rooms), and join new rooms.  

### Binary

If you are using Linux you can download the binary distribution [https://gitlab.com/s3lect/taavi-bot/-/raw/master/dist/taavi](https://gitlab.com/s3lect/taavi-bot/-/raw/master/dist/taavi) and then run the bot.
```sh
./taavi
```
On the first start it will as you for login details. If you want to use a differnt user account later just delete `📄~/.config/taavi/credentials.json` and log in again.

![](https://i.imgur.com/DLht0RF.png)

### From Source

To run the bot from the source files, clone this repository, install all dependencies and start it with npm start.
1. Clone the repo
	```sh
	git clone https://github.com/github_username/repo_name.git
	```
2. Install NPM packages
	```sh
	npm ci
	```
2. run the bot
	```sh
	npm start
	```
	or
	```sh
	node src/index.js
	```

### Systemd Service

```[/etc/systemd/system/taavi.service]
[Unit]
Description=taavi chat bot
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=myUser
ExecStart=/opt/taavi

[Install]
WantedBy=multi-user.target
```

<!-- USAGE EXAMPLES -->
## Usage

To use the bot, address him in a chat with `@` or just write the bots name at the start of your message.
```
taavi: what is the matrix protocol? 
```
This assumes that the Matrix account of your bot is called `taavi`.

## Building a binary

To create a binary distribution for your operating system [use the `pck` command](https://github.com/vercel/pkg). 
```sh
npm i
npx pkg package.json --targets node14-macos-x64 --out-path dist
```
## Motivation and Roadmap

I always wanted to write a chatbot that does cool things, so checkmark for that one on the list. In addition to that I recently failed a job interview. For their technical skill test (I did not fail this :P), I got a task for which I learned how to use `puppeteer`, then saw a post on reddit about [Tuxi](https://www.reddit.com/r/linux/comments/lk1il3/no_need_of_alexa_siri_cortana_i_am_happy_with_my/) and together with my previous project that included Matrix ([Audius](https://github.com/select/audius)), I knew what I had to do: create Taavi.

I have created this bot as a "weekend project", litteraly one day for the code and one day for documentation. Surely it's far from perfect but I don't have any big plans with it so far… If you think I should add some features, [create an issue](https://gitlab.com/s3lect/taavi-chat-bot/-/issues) and let me know. I could imagine adding other websites / services to it like last.fm, do you have any ideas?

I gave my daugther the task to choose a name for a bot, that can anser any question in the world, and she came up with Taavi, nice!

I'm currently looking for a job, preferrably in open source. Do you know anything


## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request


## License

Distributed under the MIT License. See `LICENSE` for more information.


## Contact

select - [@select:matrix.org](https://matrix.to/#/@select:matrix.org)

Project Link: [https://gitlab.com/s3lect/taavi-chat-bot](https://gitlab.com/s3lect/taavi-chat-bot)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

Thanks to the reddit post by Bugswriter I got inspired by his project. 
* [Tuxi](https://github.com/Bugswriter/tuxi)

